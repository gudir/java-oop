class Person {
    String name;
    String address;
    String pekerjaan;
    final String country = "Indonesia";

    //Constructor
    Person(String paramPekerjaan) {
        pekerjaan = paramPekerjaan;
    }

    //bisa multiple constructor asal jumlah parameter nya beda
    Person() {}

    void sayHello(String paramName) {
        System.out.println("Hello "+ paramName + ", My Name is "+name+ " Perkerjaan "+this.pekerjaan);
    }
}
