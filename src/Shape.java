class Shape {
    int getCorner() {
        return 10;
    }
}

class Rectangle extends Shape {
    //deklarasi ulang method getCorner dari parent
    int getCorner() {
        return 4;
    }
    // jika ingin mendapatkan nilai original getCorner dari parent
    int getParentCorner() {
        //untuk memanggilnya harus dengan object super
        return super.getCorner();
    }
}
