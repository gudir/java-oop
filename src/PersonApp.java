public class PersonApp {
    public static void main(String[] args) {
        //cara penulisan object
        //cara pertama
        var person1 = new Person("swasta");
        //cara kedua
        Person person2 = new Person("IRT");
        //cara ketiga
        Person person3;
        person3 = new Person("PNS");

        //cara assign value class
        person1.name = "cio";
        person1.address = "nanjung";
        //person1.country = "cobalagi"; //tidak bisa di assign value karena final
        System.out.println(person1.name);
        System.out.println(person1.address);
        System.out.println(person1.country);
        person1.sayHello("Wkwkw");
    }
}
