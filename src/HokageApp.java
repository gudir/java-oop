public class HokageApp {
    public static void main(String[] args) {
        Hokage hokage1 = new Hokage();
        hokage1.name = "Hashirama Senju";
        hokage1.sayName("Hashirama");

        WakilHokage wakilhokage1 = new WakilHokage();
        wakilhokage1.name = "Tobirama Senju";
        wakilhokage1.sayName("Tobirama");
    }
}
