//child dari Hokage extend parent Hokage
class WakilHokage extends Hokage{
    //method overriding, jadi function/method sayName di parent akan ditimpah/di deklarasi ulang ,
    // tapi hanya di child nya saja
    void sayName(String name) {
        System.out.println("Hi my name is "+name+", I am is the Vice Hokage my name is "+this.name);
    }
}
